##------DISCRIMINENT ANALYSIS-------##
dp1<-read.csv(file.choose(),header = TRUE)
dp2<-read.csv(file.choose(),header = TRUE)
dp1
dp2
attach(dp1)
attach(dp2)
x_bar<-colMeans(dp1)
x1_bar<-colMeans(dp2)
s1<-cov(dp1)
s1
s2<-cov(dp2)
s2
N1<-length(col(dp1[1]))
N2<-length(col(dp2[1]))

sp=(((N1-1)*s1)+((N2-1)*s2))/(N1+N2-2)
sp_inv=solve(sp)
sp_inv
m = 1/2*(t(x_bar-x1_bar)%*%sp_inv%*%(x_bar+x1_bar))
m
#(i)
X0=matrix(c(7.73,10.85,3.62,0.99),nrow = 4)
t1=t(x_bar-x1_bar)%*%sp_inv%*%X0
t1
if(t1>=m){"Allocate to P1"}else{"Allocate to P2"}
#(ii)
X1=matrix(c(8.69,12.05,3.11,1.31),nrow = 4)
t2=t(x_bar-x1_bar)%*%sp_inv%*%X1
t2
if(t2>=m){"Allocate to P1"}else{"Allocate to P2"}
#(iii)
X2=matrix(c(9.22,11.50,3.22,1.06),nrow = 4)
t3=t(x_bar-x1_bar)%*%sp_inv%*%X2
t3
if(t3>=m){"Allocate to P1"}else{"Allocate to P2"}

