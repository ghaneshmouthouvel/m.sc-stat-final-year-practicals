## One missing treatments in LSD

lsddt<-read.csv(file.choose(),header=TRUE)
lsddt
names(lsddt)
aovwom<-aov(lsddt$values~lsddt$rows+lsddt$columns+lsddt$treatments)
aovwom
nmiss=sum(length(which(is.na(lsddt$values))))
class(lsddt$rows)
class(lsddt$columns)
class(lsddt$treatments)
class(lsddt$values)
rows<-as.factor(lsddt$rows)
columns<-as.factor(lsddt$columns)
treatments<-as.factor(lsddt$treatments)
class(rows)
class(columns)
class(treatments)
levels(rows)
levels(columns)
levels(treatments)
r<-nlevels(rows)
c<-nlevels(columns)
t<-nlevels(treatments)
r
c
t
fmi<-match(NA,lsddt$values)
fmi
fmr<-rows[fmi]
fmr
fmc<-columns[fmi]
fmc
fmt<-treatments[fmi]
fmt
lsddt$values[fmi]=0
library(pivottabler)
table<-qpvt(lsddt,"rows","columns","sum(values)")
table
class(table)
dmat<-table$asDataMatrix()
class(dmat)
dmat
##for treatments
table1<-qpvt(lsddt,"treatments","columns","sum(values)")
table1
class(table1)
dmat1<-table1$asDataMatrix()
class(dmat1)
dmat1
## calculation of missing values
g<-dmat["Total","Total"]
r1<-dmat[fmr,"Total"]
c1<-dmat["Total",fmc]
g
r1
c1
t1<-dmat1[fmt,"Total"]
t1

x=((t*(r1+c1+t1))-(2*g))/((t-1)*(t-2))
x
lsddt$values[fmi]=x
lsddt
attach(lsddt)
##Anova table for Lsd
lsdaov<-aov(values~rows+columns+treatments)
summary(lsdaov)

##Finding f values with modified df
(res_sq=summary(lsdaov)[[1]][["Sum Sq"]][4])
(res_df=summary(lsdaov)[[1]][["Df"]][4])
#res_df=rbdaov$df.residual
(updatedres_df=res_df-nmiss)
(rows_msq=summary(lsdaov)[[1]][["Mean Sq"]][1])
(columns_msq=summary(lsdaov)[[1]][["Mean Sq"]][2])
(treatments_msq=summary(lsdaov)[[1]][["Mean Sq"]][3])
(updres_msq=res_sq/updatedres_df)
(F_rows = rows_msq/updres_msq)
(F_columns = columns_msq/updres_msq)
(F_treatments = treatments_msq/updres_msq)
(df_rows = summary(lsdaov)[[1]][["Df"]][1])
(df_columns = summary(lsdaov)[[1]][["Df"]][2])
(df_treatments = summary(lsdaov)[[1]][["Df"]][3])
(pvalue_rows=1-pf(F_rows,df_rows,updatedres_df))
(pvalue_columns=1-pf(F_columns,df_columns,updatedres_df))
(pvalue_treatments=1-pf(F_treatments,df_treatments,updatedres_df))
##Exact test
Bias=(((t*(r1+c1+t1))-(2*g))**2)/(((t-1)*(t-2))**2)
Bias
SSTud<-summary(lsdaov)[[1]][["Sum Sq"]][[3]]
SSTad=(SSTud-Bias)
SSTad
