 ## two missing treatments in LSD

lsddt1<-read.csv(file.choose(),header=TRUE)
lsddt1
names(lsddt1)
aovwom<-aov(lsddt1$values~lsddt1$rows+lsddt1$columns+lsddt1$treatments)
summary(aovwom)
nmiss=sum(length(which(is.na(lsddt1$values))))
class(lsddt1$rows)
class(lsddt1$columns)
class(lsddt1$treatments)
class(lsddt1$values)
rows<-as.factor(lsddt1$rows)
columns<-as.factor(lsddt1$columns)
treatments<-as.factor(lsddt1$treatments)
class(rows)
class(columns)
class(treatments)
levels(rows)
levels(columns)
levels(treatments)
r<-nlevels(rows)
c<-nlevels(columns)
t<-nlevels(treatments)
r
c
t
fmi<-match(NA,lsddt1$values)
fmi
fmr<-rows[fmi]
fmr
fmc<-columns[fmi]
fmc
fmt<-treatments[fmi]
fmt
lsddt1$values[fmi]=0
smi<-match(NA,lsddt1$values)
smi
smr<-rows[smi]
smr
smc<-columns[smi]
smc
smt<-treatments[smi]
smt
lsddt1$values[smi]=0
library(pivottabler)
table<-qpvt(lsddt1,"rows","columns","sum(values)")
table
class(table)
dmat<-table$asDataMatrix()
class(dmat)
dmat
##for treatments
table1<-qpvt(lsddt1,"columns","treatments","sum(values)")
table1
class(table1)
dmat1<-table1$asDataMatrix()
class(dmat1)
dmat1
## calculation of missing values
g<-dmat["Total","Total"]
r1<-dmat[fmr,"Total"]
c1<-dmat["Total",fmc]
g
r1
c1
r2<-dmat[smr,"Total"]
c2<-dmat["Total",smc]
t1<-dmat1["Total",fmt]
t1
t2<-dmat1["Total",smt]
t2
r2
c2
x=((t*(r1+c1+t1))-(2*(g+y)))/((t-1)*(t-2))
x
y=((t*(r2+c2+t2))-(2*(g+x)))/((t-1)*(t-2))
y

lsddt1$values[fmi]=x
lsddt1$values[smi]=y
lsddt1
attach(lsddt1)
##Anova table for Lsd
lsdaov<-aov(values~rows+columns+treatments)
summary(lsdaov)

##Finding f values with modified df
(res_sq=summary(lsdaov)[[1]][["Sum Sq"]][4])
(res_df=summary(lsdaov)[[1]][["Df"]][4])
#res_df=rbdaov$df.residual
(updatedres_df=res_df-nmiss)
(rows_msq=summary(lsdaov)[[1]][["Mean Sq"]][1])
(columns_msq=summary(lsdaov)[[1]][["Mean Sq"]][2])
(treatments_msq=summary(lsdaov)[[1]][["Mean Sq"]][3])
(updres_msq=res_sq/updatedres_df)
(F_rows = rows_msq/updres_msq)
(F_columns = columns_msq/updres_msq)
(F_treatments = treatments_msq/updres_msq)
(df_rows = summary(lsdaov)[[1]][["Df"]][1])
(df_columns = summary(lsdaov)[[1]][["Df"]][2])
(df_treatments = summary(lsdaov)[[1]][["Df"]][3])
(pvalue_rows=1-pf(F_rows,df_rows,updatedres_df))
(pvalue_columns=1-pf(F_columns,df_columns,updatedres_df))
(pvalue_treatments=1-pf(F_treatments,df_treatments,updatedres_df))