Data<-read.csv(file = "C:\\Users\\Naresh\\OneDrive\\Desktop\\All files\\DOE\\prac codes\\3^2 data.csv",header=TRUE)
Data
attach(Data)
Replicate<-as.factor(Replicate)
Dstrength<-as.factor(Dstrength)
Dtime<-as.factor(Dtime)
aov_1<-aov(Response~Replicate+Dstrength*Dtime)
k<-anova(aov_1)
k

library(pivottabler)
table=qpvt(Data,rows="Dstrength",columns="Dtime",calculations="sum(Response)")
dmat<-table$asDataMatrix()
dmat
ab_0<-dmat[1,1]+dmat[2,3]+dmat[3,2]
ab_1<-dmat[1,2]+dmat[2,1]+dmat[3,3]
ab_2<-dmat[1,3]+dmat[2,2]+dmat[3,1]
ssab<-((ab_0^2+ab_1^2+ab_2^2)/(3*4))-(((dmat["Total","Total"])^2)/(9*4))
ab2_0<-dmat[1,1]+dmat[2,2]+dmat[3,3]
ab2_1<-dmat[1,3]+dmat[2,1]+dmat[3,2]
ab2_2<-dmat[2,1]+dmat[2,3]+dmat[3,1]
ssab2<-((ab2_0^2+ab2_1^2+ab2_2^2)/(3*4))-(((dmat["Total","Total"])^2)/(9*4))
krs=k[5,]
krs
k=rbind(k,krs)
k[4,1]=2
k[4,2]=ssab
k[4,3]=ssab/2
k[4,4]=k[4,3]/k[5,3]
k[4,5]=pf(k[4,4],2,24,lower.tail = F)
k
row.names(k)=c("Replicate","Dstrength","Dtime","Dstrength*Dtime","(Dstrength*Dtime)**2","residuals")
k
k[5,1]=2
k[5,2]=ssab2
k[5,3]=ssab2/2
k[5,4]=k[5,3]/k[6,3]
k[5,5]=pf(k[5,4],2,24,lower.tail = F)
k

